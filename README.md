# Education Center Management

This is a graduation project of FPT University

## Authors

- [@khacviet](https://www.facebook.com/kv.99kv)
- [@manhkien](#)
- [@congphong](#)
- [@anhtuan](#)
- [@tuyetmai](#)

## Tech Stack

**Client:** React, Redux, Material-UI

**Server:** Node, Express, PostgreSQL

**Tools:** Gitlab CI/CD, AWS, Docker

## Demo

Click [xoaicms.site](https://xoaicms.site) to open Education Center CMS  
Click [xoaieducation.site](https://xoaieducation.site) to open Education Center Website  
Account: `admin`  
Password: `123123asd`

## Screenshots

![alt text](./screenshots/img1.png)
![alt text](./screenshots/img2.png)
![alt text](./screenshots/img3.png)
![alt text](./screenshots/img4.png)
![alt text](./screenshots/img5.png)
